/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : lamp_shop

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-08-24 14:23:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `pwd` char(32) NOT NULL COMMENT '密码',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1-激活 2-禁用',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '1', '1470675003');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `pid` int(11) NOT NULL COMMENT '父级id',
  `path` varchar(255) NOT NULL COMMENT '分类路径信息',
  `display` tinyint(1) DEFAULT '2' COMMENT '1-显示 2-隐藏',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '华为专区', '0', '0,', '1');
INSERT INTO `category` VALUES ('2', '荣耀专区', '0', '0,', '1');
INSERT INTO `category` VALUES ('14', '平板专区', '0', '0,', '1');
INSERT INTO `category` VALUES ('20', '3', '1', '0,1,', '2');
INSERT INTO `category` VALUES ('18', '1', '1', '0,1,', '2');
INSERT INTO `category` VALUES ('8', 'HUAWEI 黑科技', '0', '0,', '1');
INSERT INTO `category` VALUES ('9', '别点我~', '0', '0,', '1');
INSERT INTO `category` VALUES ('11', 'WATCH', '0', '0,', '1');
INSERT INTO `category` VALUES ('12', '配件特惠', '0', '0,', '1');
INSERT INTO `category` VALUES ('19', '2', '1', '0,1,', '2');
INSERT INTO `category` VALUES ('21', '4', '1', '0,1,', '2');
INSERT INTO `category` VALUES ('22', '123', '18', '0,1,18,', '2');
INSERT INTO `category` VALUES ('23', '77', '2', '0,2,', '2');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL COMMENT '分类id',
  `name` varchar(100) NOT NULL COMMENT '商品名',
  `price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `stock` int(11) NOT NULL COMMENT '库存',
  `sales` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `up` tinyint(1) DEFAULT '1' COMMENT '1-上架  2-下架',
  `hot` tinyint(1) DEFAULT '2' COMMENT '1-热销  2-非热销',
  `desc` varchar(255) NOT NULL COMMENT '描述',
  `addtime` int(10) NOT NULL COMMENT '添加时间',
  `uptime` int(10) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', '4', '荣耀V8', '2699.00', '200', '0', '1', '1', '&lt;span style=&quot;color:#E01D20;font-family:Tahoma, 微软雅黑;font-size:16px;line-height:20px;background-color:#FFFFFF;&quot;&gt;精致外观，双1200万像素平行镜头，5.7英寸FHD屏，搭载麒麟950系列芯片，3500mAh电池长久续航；如有异见，不如一见！&lt;/span&gt;&lt;br /&gt;', '1471096622', null);
INSERT INTO `goods` VALUES ('2', '4', '荣耀V8', '2555.00', '44', '0', '1', '1', '你值得拥有！', '1471103389', null);
INSERT INTO `goods` VALUES ('3', '4', '华为', '111.00', '111', '0', '2', '2', '很好', '1471151786', null);
INSERT INTO `goods` VALUES ('23', '12', '荣耀小口哨蓝牙耳机', '189.00', '999', '0', '1', '2', '', '1471362902', null);
INSERT INTO `goods` VALUES ('5', '4', 'HUAWEI P9', '3988.00', '30', '0', '1', '1', '一上手就爱不释手', '1471265032', null);
INSERT INTO `goods` VALUES ('22', '12', '荣耀引擎耳机PLUS', '79.00', '999', '0', '1', '2', '完美音质', '1471362600', null);
INSERT INTO `goods` VALUES ('27', '1', '华为畅想5S', '999.00', '999', '0', '1', '1', '买即送自拍杆+保护壳', '1471431560', null);
INSERT INTO `goods` VALUES ('9', '11', '华为手环 B3 ', '1199.00', '99', '0', '1', '2', '', '1471339397', null);
INSERT INTO `goods` VALUES ('10', '11', '荣耀畅玩手环 A1 ', '129.00', '99', '0', '1', '2', '', '1471339632', null);
INSERT INTO `goods` VALUES ('15', '11', 'HUAWEI WATCH', '1988.00', '998', '0', '1', '2', '', '1471360929', null);
INSERT INTO `goods` VALUES ('17', '1', 'HUAWEI Mate 8', '2788.00', '777', '0', '1', '1', '高性能与长续航的结合', '1471361343', null);
INSERT INTO `goods` VALUES ('11', '11', '华为手环 B2', '999.00', '99', '0', '1', '2', '', '1471339785', null);
INSERT INTO `goods` VALUES ('13', '1', 'HUAWEI 麦芒5', '2399.00', '99', '0', '1', '1', '华为 卡莱联合设计', '1471349072', null);
INSERT INTO `goods` VALUES ('16', '1', 'HUAWEI P9 Plus', '4338.00', '77', '0', '1', '1', '华为 卡莱联合设计', '1471361180', null);
INSERT INTO `goods` VALUES ('14', '14', '华为（HUAWEI） M2 10.0 平板电脑 LTE 月光银', '2688.00', '199', '0', '1', '2', '', '1471355557', null);
INSERT INTO `goods` VALUES ('29', '2', '荣耀8', '2888.00', '999', '0', '1', '2', '', '1471432372', null);
INSERT INTO `goods` VALUES ('28', '2', '荣耀8', '2588.00', '999', '0', '1', '1', '', '1471432121', null);
INSERT INTO `goods` VALUES ('20', '2', '荣耀畅玩5c', '899.00', '899', '0', '1', '1', '16纳米芯 真&quot;芯&quot;长续航', '1471361816', null);
INSERT INTO `goods` VALUES ('21', '2', '荣耀7', '1779.00', '999', '0', '1', '1', '智灵键 创新语音控制', '1471361896', null);
INSERT INTO `goods` VALUES ('26', '14', '华为揽阅M2 青春版 LTE版 16GB （珍珠白）', '1899.00', '999', '0', '1', '2', '极速体验首选', '1471364111', null);
INSERT INTO `goods` VALUES ('24', '12', '荣耀移动电源', '125.00', '999', '0', '1', '2', '10000mAh 超大容量', '1471363088', null);
INSERT INTO `goods` VALUES ('25', '12', '华为充电器', '68.00', '999', '0', '1', '2', '打造精品', '1471363373', null);
INSERT INTO `goods` VALUES ('30', '8', '华为盒子', '111.00', '111', '0', '1', '2', '因为黑所以黑', '1471800379', null);
INSERT INTO `goods` VALUES ('31', '9', '华为笔记本', '3333.00', '444', '0', '1', '2', '政府指定采购商品', '1471800507', null);
INSERT INTO `goods` VALUES ('40', '1', '&lt;a href=&quot;http://www.baidu.com&quot;&gt;百度&lt;/a&gt;', '123.00', '123', '0', '1', '2', '&amp;lt;a href=&quot;http://www.baidu.com&quot;&amp;gt;百度&amp;lt;/a&amp;gt;', '1471933821', null);
INSERT INTO `goods` VALUES ('41', '18', '&lt;b&gt;b&lt;/b&gt;', '20.00', '2', '0', '2', '1', '&amp;lt;b&amp;gt;b&amp;lt;/b&amp;gt;&amp;lt;b&amp;gt;b&amp;lt;/b&amp;gt;', '1471938324', null);
INSERT INTO `goods` VALUES ('42', '1', '&lt;b&gt;b&lt;/b&gt;', '20.00', '20', '0', '1', '1', '&amp;lt;b&amp;gt;b&amp;lt;/b&amp;gt;&amp;lt;b&amp;gt;b&amp;lt;/b&amp;gt;&amp;lt;b&amp;gt;b&amp;lt;/b&amp;gt;', '1471938363', null);

-- ----------------------------
-- Table structure for goodsimg
-- ----------------------------
DROP TABLE IF EXISTS `goodsimg`;
CREATE TABLE `goodsimg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL COMMENT '商品id',
  `icon` varchar(255) NOT NULL COMMENT '商品图片',
  `face` tinyint(1) DEFAULT '2' COMMENT '商品封面 1-封面  2-非封面',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goodsimg
-- ----------------------------
INSERT INTO `goodsimg` VALUES ('4', '3', '2016081457b021008c9bb.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('16', '7', '2016081557b1d21591517.png', '2');
INSERT INTO `goodsimg` VALUES ('8', '4', '2016081457b043a44b083.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('15', '7', '2016081557b1bcb000f53.png', '2');
INSERT INTO `goodsimg` VALUES ('14', '7', '', '2');
INSERT INTO `goodsimg` VALUES ('12', '6', '', '2');
INSERT INTO `goodsimg` VALUES ('13', '6', '2016081557b1b92069a90.png', '1');
INSERT INTO `goodsimg` VALUES ('17', '7', '2016081557b1d2323f32f.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('101', '5', '2016082257ba97ffd6895.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('19', '7', '2016081557b1d55420677.png', '1');
INSERT INTO `goodsimg` VALUES ('20', '8', '', '2');
INSERT INTO `goodsimg` VALUES ('21', '8', '2016081557b1dba228c85.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('25', '10', '2016081657b2dc87c162e.png', '2');
INSERT INTO `goodsimg` VALUES ('27', '11', '2016081657b2dd1def6dc.png', '2');
INSERT INTO `goodsimg` VALUES ('28', '12', '', '2');
INSERT INTO `goodsimg` VALUES ('29', '12', '2016081657b2ddc0f03d7.png', '1');
INSERT INTO `goodsimg` VALUES ('30', '6', '2016081657b2e61ef1193.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('32', '13', '2016081657b3017db5786.png', '2');
INSERT INTO `goodsimg` VALUES ('102', '30', '2016082257ba982fddf2e.png', '2');
INSERT INTO `goodsimg` VALUES ('34', '14', '2016081657b31ab4e7927.png', '2');
INSERT INTO `goodsimg` VALUES ('94', '16', '2016082257ba974446029.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('36', '15', '2016081657b32fb30f99d.png', '1');
INSERT INTO `goodsimg` VALUES ('95', '17', '2016082257ba97557f230.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('38', '16', '2016081657b330c90ca59.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('96', '21', '2016082257ba976b9a63c.png', '2');
INSERT INTO `goodsimg` VALUES ('40', '17', '2016081657b3314dd6356.png', '1');
INSERT INTO `goodsimg` VALUES ('41', '18', '', '2');
INSERT INTO `goodsimg` VALUES ('42', '18', '2016081657b33243f24db.png', '1');
INSERT INTO `goodsimg` VALUES ('43', '19', '', '2');
INSERT INTO `goodsimg` VALUES ('46', '21', '2016081657b333be4f212.png', '1');
INSERT INTO `goodsimg` VALUES ('47', '20', '2016081657b333e67c9cd.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('48', '19', '2016081657b33402c5077.png', '2');
INSERT INTO `goodsimg` VALUES ('97', '20', '2016082257ba97aa30ad2.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('100', '24', '2016082257ba97d9c4bff.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('53', '23', '2016081757b33a4d8f59a.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('54', '25', '2016081757b33a89e46be.png', '1');
INSERT INTO `goodsimg` VALUES ('55', '24', '2016081757b33ac0c6ea1.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('56', '22', '2016081757b33adb227cb.png', '1');
INSERT INTO `goodsimg` VALUES ('58', '26', '2016081757b33c2d0ae2a.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('59', '5', '2016081757b3e01b2acb2.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('62', '9', '2016081757b40e88d88ae.png', '1');
INSERT INTO `goodsimg` VALUES ('63', '9', '2016081757b40e9a6e193.png', '2');
INSERT INTO `goodsimg` VALUES ('64', '11', '2016081757b40eaf753a6.png', '2');
INSERT INTO `goodsimg` VALUES ('65', '11', '2016081757b40eba84c36.png', '1');
INSERT INTO `goodsimg` VALUES ('66', '10', '2016081757b40ecfcb5f8.png', '1');
INSERT INTO `goodsimg` VALUES ('67', '10', '2016081757b40eda4ec38.png', '2');
INSERT INTO `goodsimg` VALUES ('68', '13', '2016081757b40ef2c5c21.png', '1');
INSERT INTO `goodsimg` VALUES ('69', '13', '2016081757b40f03e7eed.png', '2');
INSERT INTO `goodsimg` VALUES ('93', '15', '2016082257ba8809c9107.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('71', '14', '2016081757b419f51d597.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('72', '22', '2016081757b41a092d7fa.png', '2');
INSERT INTO `goodsimg` VALUES ('73', '26', '2016081757b41a1f8efac.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('74', '23', '2016081757b41a3007390.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('103', '27', '2016082257ba98419c614.png', '2');
INSERT INTO `goodsimg` VALUES ('86', '30', '2016082257b9e453ec4e7.png', '1');
INSERT INTO `goodsimg` VALUES ('78', '27', '2016081757b443b568def.png', '1');
INSERT INTO `goodsimg` VALUES ('79', '19', '2016081757b4445ab853f.png', '1');
INSERT INTO `goodsimg` VALUES ('108', '28', '2016082257ba99cb4d053.png', '2');
INSERT INTO `goodsimg` VALUES ('81', '28', '2016081757b445df0cfe1.png', '1');
INSERT INTO `goodsimg` VALUES ('116', '42', '', '1');
INSERT INTO `goodsimg` VALUES ('107', '29', '2016082257ba99bbcd38e.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('84', '29', '2016081757b446c44bc39.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('106', '31', '2016082257ba99aa9c65e.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('88', '31', '2016082257b9e4c61b249.jpeg', '1');
INSERT INTO `goodsimg` VALUES ('114', '40', '', '1');
INSERT INTO `goodsimg` VALUES ('91', '15', '2016082257ba5bdca4da3.jpeg', '2');
INSERT INTO `goodsimg` VALUES ('115', '41', '', '1');
INSERT INTO `goodsimg` VALUES ('99', '25', '2016082257ba97cb600e0.png', '2');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderNum` varchar(255) NOT NULL COMMENT '订单编号',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `receiver` varchar(50) NOT NULL COMMENT '收货人',
  `address` varchar(255) NOT NULL COMMENT '收货地址',
  `phone` char(11) NOT NULL COMMENT '联系电话',
  `amount` decimal(10,2) NOT NULL COMMENT '总价',
  `time` int(10) NOT NULL COMMENT '下单时间',
  `orderWay` tinyint(1) DEFAULT '1' COMMENT '支付方式 1-货到付款 2-在线支付',
  `status` tinyint(1) DEFAULT '1' COMMENT '发货状态 1-未发货 2-已发货  3-已收货',
  `isPay` tinyint(1) DEFAULT '2' COMMENT '是否支付 1-未支付 2-已支付',
  `cancel` tinyint(1) DEFAULT '1' COMMENT '取消状态 1-未取消 2-已取消',
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderNum` (`orderNum`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('64', '3b400c92-285f-7436-257c-cda07ef7be41', '22', '李华威', '&lt;a href =&quot;www.baidu.com&quot;&gt;&lt;/a&gt;', '15515515555', '2688.00', '1471835365', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('63', '9257747d-4333-e81c-2bbd-9ecc4d889f00', '22', '123', '123', '15515515555', '2788.00', '1471834474', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('62', '0dc06679-b39d-8082-8870-dfb4ac13822f', '22', '123', '123', '123', '4338.00', '1471834250', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('61', '90dd0529-98f9-7ec5-a378-0b51c4204399', '22', '', '', '', '4338.00', '1471834137', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('60', '99c82b42-1cdd-c570-bf2d-8f3ec16d5e29', '22', ' ', ' ', '122`', '125.00', '1471833844', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('59', 'daaf73d9-1004-82b1-5212-19e8e81c138b', '22', '&lt;a&gt;www.baidu.com&lt;/a&gt;', '', '', '4338.00', '1471833732', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('58', '30398f71-29b2-6682-a072-d562f66082d4', '22', '123', '123', '123', '899.00', '1471833719', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('57', 'ea52cc72-dd2f-15d2-5bb8-d740a0a1cd79', '22', '', '', '', '7026.00', '1471833686', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('56', 'ff70ede3-d59e-9b21-c7d6-57f08825efb8', '22', '123', '', '123', '3333.00', '1471833311', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('77', 'f6d9d026-92e2-1aee-5748-216faabaa2ef', '20', '小小', '上海', '15888881234', '4232.00', '1471886019', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('55', '708f89e3-5055-4d0b-a538-b63828ce284b', '22', '', '&lt;a&gt;www.baidu.com&lt;/a&gt;', '', '2788.00', '1471832797', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('71', 'faa37faa-2380-b4aa-b654-58d85aabb505', '30', '李华威', '上海', '15515515555', '2788.00', '1471860350', '2', '2', '1', '1');
INSERT INTO `orders` VALUES ('74', '75a37c9d-fdc6-5b49-bd14-72aa1e964c30', '30', '李华威', '上海', '15515515555', '79.00', '1471860746', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('75', '2028f1c7-e627-93f3-d6d3-a174a710d6f4', '30', '熊大', ' 河南', '13839999539', '111.00', '1471865588', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('72', 'a0f98a25-c2c1-eed6-70fe-919b44c83008', '30', '李华威', '上海', '15515515555', '4338.00', '1471860467', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('73', 'a8daf18d-f07e-b355-e076-b7438adb8d83', '30', '李华威', '上海', '15515515555', '3333.00', '1471860518', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('76', '99ae4898-2d78-8b73-2f32-541afa7b8469', '26', 'AAA', '上海', '15515515555', '2588.00', '1471869895', '1', '2', '2', '1');
INSERT INTO `orders` VALUES ('78', '77bfdca9-569e-9966-4164-060a73e8400d', '26', '张圣侍', '上海', '13723248694', '28880.00', '1471915516', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('79', '72893a4c-f211-cbe1-6393-449869608c81', '26', '张圣侍', '安徽', '15037847256', '129.00', '1471917549', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('80', '04d173e6-530a-5104-3f01-db36e03188e0', '20', '李华威', '上海', '15515515555', '111.00', '1471934865', '1', '1', '2', '1');
INSERT INTO `orders` VALUES ('81', '53f18be1-67a3-4eeb-1a74-fda684f73b79', '20', '&lt;b&gt;b&lt;/b&gt;', '&lt;b&gt;b&lt;/b&gt;', '15515515555', '11052.00', '1471938536', '1', '2', '2', '1');

-- ----------------------------
-- Table structure for ordersgood
-- ----------------------------
DROP TABLE IF EXISTS `ordersgood`;
CREATE TABLE `ordersgood` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL COMMENT '订单id',
  `gid` int(11) NOT NULL COMMENT '商品id',
  `price` decimal(10,2) NOT NULL COMMENT '单价',
  `count` int(11) NOT NULL COMMENT '数量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ordersgood
-- ----------------------------
INSERT INTO `ordersgood` VALUES ('1', '13', '14', '2688.00', '2');
INSERT INTO `ordersgood` VALUES ('2', '13', '20', '899.00', '2');
INSERT INTO `ordersgood` VALUES ('3', '14', '14', '2688.00', '2');
INSERT INTO `ordersgood` VALUES ('4', '14', '20', '899.00', '2');
INSERT INTO `ordersgood` VALUES ('5', '15', '14', '2688.00', '2');
INSERT INTO `ordersgood` VALUES ('6', '15', '20', '899.00', '2');
INSERT INTO `ordersgood` VALUES ('7', '16', '14', '2688.00', '2');
INSERT INTO `ordersgood` VALUES ('8', '16', '20', '899.00', '2');
INSERT INTO `ordersgood` VALUES ('9', '17', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('10', '18', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('11', '19', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('12', '19', '27', '999.00', '1');
INSERT INTO `ordersgood` VALUES ('13', '20', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('14', '20', '27', '999.00', '1');
INSERT INTO `ordersgood` VALUES ('15', '20', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('16', '21', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('17', '22', '17', '2788.00', '2');
INSERT INTO `ordersgood` VALUES ('18', '23', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('19', '24', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('20', '24', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('21', '25', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('22', '25', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('23', '26', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('24', '26', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('25', '27', '28', '2588.00', '2');
INSERT INTO `ordersgood` VALUES ('26', '28', '28', '2588.00', '2');
INSERT INTO `ordersgood` VALUES ('27', '28', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('28', '29', '28', '2588.00', '2');
INSERT INTO `ordersgood` VALUES ('29', '29', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('30', '31', '28', '2588.00', '2');
INSERT INTO `ordersgood` VALUES ('31', '31', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('32', '32', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('33', '33', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('34', '34', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('35', '35', '29', '2888.00', '1');
INSERT INTO `ordersgood` VALUES ('36', '36', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('37', '37', '26', '1899.00', '1');
INSERT INTO `ordersgood` VALUES ('38', '38', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('39', '39', '29', '2888.00', '1');
INSERT INTO `ordersgood` VALUES ('40', '40', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('41', '41', '24', '125.00', '1');
INSERT INTO `ordersgood` VALUES ('42', '42', '31', '3333.00', '1');
INSERT INTO `ordersgood` VALUES ('43', '43', '15', '1988.00', '1');
INSERT INTO `ordersgood` VALUES ('44', '43', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('45', '43', '31', '3333.00', '1');
INSERT INTO `ordersgood` VALUES ('46', '44', '28', '2588.00', '1');
INSERT INTO `ordersgood` VALUES ('47', '45', '25', '68.00', '1');
INSERT INTO `ordersgood` VALUES ('48', '46', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('49', '47', '9', '1199.00', '1');
INSERT INTO `ordersgood` VALUES ('50', '48', '21', '1779.00', '1');
INSERT INTO `ordersgood` VALUES ('51', '49', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('52', '50', '30', '111.00', '1');
INSERT INTO `ordersgood` VALUES ('53', '51', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('54', '52', '23', '189.00', '1');
INSERT INTO `ordersgood` VALUES ('55', '53', '30', '111.00', '1');
INSERT INTO `ordersgood` VALUES ('56', '54', '20', '899.00', '-1');
INSERT INTO `ordersgood` VALUES ('57', '55', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('58', '56', '31', '3333.00', '1');
INSERT INTO `ordersgood` VALUES ('59', '57', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('60', '57', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('61', '58', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('62', '59', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('63', '60', '24', '125.00', '1');
INSERT INTO `ordersgood` VALUES ('64', '61', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('65', '62', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('66', '63', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('67', '64', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('68', '65', '30', '111.00', '3');
INSERT INTO `ordersgood` VALUES ('69', '66', '25', '68.00', '1');
INSERT INTO `ordersgood` VALUES ('70', '67', '13', '2399.00', '8');
INSERT INTO `ordersgood` VALUES ('71', '68', '13', '2399.00', '1');
INSERT INTO `ordersgood` VALUES ('72', '69', '28', '2588.00', '10');
INSERT INTO `ordersgood` VALUES ('73', '69', '24', '125.00', '1');
INSERT INTO `ordersgood` VALUES ('74', '69', '22', '79.00', '1');
INSERT INTO `ordersgood` VALUES ('75', '70', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('76', '71', '17', '2788.00', '1');
INSERT INTO `ordersgood` VALUES ('77', '72', '16', '4338.00', '1');
INSERT INTO `ordersgood` VALUES ('78', '73', '31', '3333.00', '1');
INSERT INTO `ordersgood` VALUES ('79', '74', '22', '79.00', '1');
INSERT INTO `ordersgood` VALUES ('80', '75', '30', '111.00', '1');
INSERT INTO `ordersgood` VALUES ('81', '76', '28', '2588.00', '1');
INSERT INTO `ordersgood` VALUES ('82', '77', '20', '899.00', '1');
INSERT INTO `ordersgood` VALUES ('83', '77', '31', '3333.00', '1');
INSERT INTO `ordersgood` VALUES ('84', '78', '29', '2888.00', '10');
INSERT INTO `ordersgood` VALUES ('85', '79', '10', '129.00', '1');
INSERT INTO `ordersgood` VALUES ('86', '80', '30', '111.00', '1');
INSERT INTO `ordersgood` VALUES ('87', '81', '14', '2688.00', '1');
INSERT INTO `ordersgood` VALUES ('88', '81', '17', '2788.00', '3');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tel` char(11) NOT NULL COMMENT '手机号',
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `pwd` char(32) NOT NULL COMMENT '密码',
  `sex` tinyint(1) DEFAULT '1' COMMENT '1-男 2-女',
  `birthday` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `icon` varchar(50) DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-激活 2-禁用',
  `regtime` int(10) NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tel` (`tel`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('26', '15890972621', '李华威', 'AAAaaa', '1', null, 'asfd@aas', '2016082257baac71878a9.png', '1', '1471827809');
INSERT INTO `user` VALUES ('8', '13302030120', '熊大', '202cb962ac59075b964b07152d234b70', '1', '2016-08-08', '1123@qq.com', '2016081057ab399aea648.jpeg', '1', '1470838418');
INSERT INTO `user` VALUES ('9', '15037847254', '熊2', 'e267cfcd18461ce938067eca67c59f41', '1', '2016-08-07', '231@qq.com', '2016082357bba20e77348.jpeg', '1', '1470838929');
INSERT INTO `user` VALUES ('32', '13333333333', '<b>b</b>', '4297f44b13955235245b2497399d7a93', '1', '2016-02-01', '123@123', '2016082357bbfecb62116.jpeg', '1', '1471938251');
INSERT INTO `user` VALUES ('30', '13839999539', '华为', 'e267cfcd18461ce938067eca67c59f41', '1', null, '123@123', '2016082257baae37efc99.png', '1', '1471851829');
INSERT INTO `user` VALUES ('31', '13512341234', '病魔缠身', 'e267cfcd18461ce938067eca67c59f41', '1', null, null, null, '1', '1471920630');
INSERT INTO `user` VALUES ('20', '15037847256', '小小', '4297f44b13955235245b2497399d7a93', '2', '2016-08-01', '123@123', '2016082357bbffc2a9a04.jpeg', '1', '1471762454');
