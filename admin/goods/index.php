<?php 
	include '../init.php';

	$name= $_POST['name'];
	if(empty($name)){
		$sql = '
				select g.id, g.name, g.cid, g.price, g.stock, g.desc, g.sales, g.up ,g.hot, g.addtime, g.uptime, i.icon
				from  goods g, goodsImg i
				where g.id = i.gid and i.face = 1
				';
	// echo $sql;exit;
		
	}else{
	 	$sql = '
				select g.id, g.name, g.cid, g.price, g.stock, g.desc, g.sales, g.up ,g.hot, g.addtime, g.uptime, i.icon
				from  goods g, goodsImg i
				where g.id = i.gid and i.face = 1 and name like "%'.htmlentities($name).'%"
				';
	}

	$result = page($sql);
	//var_dump($result);

	$user_list =$result[1];
	$page = $result[0];




 ?>
 <!doctype html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
			<link rel="stylesheet" type="text/css" href="<?= CSS ?>index.css">
			<link rel="stylesheet" href="<?= PUB_CSS?>	commond.css">
			<link rel="stylesheet" href="<?= PUB_CSS?>iconfont/iconfont.css">
 </head>
 <body>
 	<div class="container">
	    <div id="search">
	      <form action="index.php" method='post'>
	      	 Name : <input type="text" name="name" class='search'>
	      	<input class="button" type="submit" value="Search" >
	      </form>
	    </div>
	</div>
 	<table class='table'>
 		<tr>
 			<th>图片分类</th>	
 			<th>分类编号</th>	
 			<th>商品编号</th>	
 			<th>商品名</th>	
 			<th>价格</th>	
 			<th>库存</th>	
 			<th>销量</th>	
 			<th>上架</th>	
 			<th>热销</th>	
 			<th>描述</th>	
 			<th>添加时间</th>	
 			<th>更新时间</th>	
 			<th>操作</th>		
 		</tr>
		<?php if(empty($user_list)): ?>
			<tr><td colspan=13>找不到您搜索的内容......</td></tr>
		<?php else: ?>
 		<?php foreach($user_list as $v): ?>
 			<tr>
 				<td>
 					<?php if(empty($v['icon'])): ?>
 						<img src="<?= PUB_IMG?>icon.jpg" width=50>

 					<?php else: ?>

				
					<img src="<?= img_url($v['icon'])?>" width=50>
 					<?php endif; ?>	
				
 					
 				</td>
				<td><?= $v['id']?></td>
				<td><?= $v['cid']?></td>
				<td><?= $v['name']?></td>
				<td><?= $v['price']?></td>
				<td><?= $v['stock']?></td>
				<td><?= $v['sales']?></td>
				<td><a href="action.php?bz=up&id=<?= $v['id']?>&up=<?=$v['up']?>" class='iconfont'><?= $v['up']==1?'&#xe603;':'&#xe61e;';?></a></td>
				<td><a href="action.php?bz=hot&id=<?= $v['id']?>&hot=<?=$v['hot']?>" class='iconfont'><?= $v['hot']==1?'&#xe603;':'&#xe61e;';?></a></td>

				<td><?= $v['desc']?></td>
				<td><?= date('Y-m-d H:i:s',$v['addtime'])?></td>

				<td><?= empty($V['uptime'])?'暂无更新' : date('Y-m-d H:i:s',$v['uptime'])?></td>

				<td>
					<a href="edit.php?id=<?= $v['id'] ?>">编辑</a>
					<a href="action.php?bz=del&id=<?= $v['id']?>">删除</a>
					<a href="img.php?id=<?=$v ['id']?>">管理图片</a>

				</td>	
 			</tr>
 		<?php endforeach; ?>
 	<?php endif; ?>	
 	</table>
 	<?= $page ?>
 </body>
 </html>