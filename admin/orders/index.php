<?php  
	include '../init.php';

	
	$tel = $_POST['tel'];
	

	if(empty($tel)){

		$sql = '
				select o.id, u.tel, o.orderNum, o.time, o.amount, o.status, o.isPay, o.orderWay
				from  orders o, user u
				where o.uid = u.id
				order by o.time desc
				';
	}else{
		$sql = '
				select o.id, u.tel, o.orderNum, o.time, o.amount, o.status, o.isPay, o.orderWay
				from  orders o, user u
				where o.uid = u.id and tel like "%'.htmlentities($tel).'%"
				order by o.time desc 
				';
	}
	// var_dump($tel);
	// echo $sql;

	
	$result = page($sql);
	$order_list = $result[1];
	$page = $result[0];
	// var_dump($order_list);
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?= PUB_CSS?>commond.css">
	<link rel="stylesheet" href="<?= PUB_CSS?>iconfont/iconfont.css">
</head>
<body>
	<div class="container">
	    <div id="search">
	      <form action="index.php" method='post'>
	      	 TEL : <input type="text" name="tel" class='search'>
	      	<input class="button" type="submit" value="Search" >
	      </form>
	    </div>
	</div>
	<table class='table'>
		<tr>
			<th>编号</th>
			<th>手机号</th>
			<th>订单号</th>
			<th>下单时间</th>
			<th>成交金额</th>
			<th>发货状态</th>
			<th>支付状态</th>
			<th>付款方式</th>
			<th>操作</th>
		</tr>
		<?php if(empty($order_list)): ?>
			<tr><td colspan=13>找不到您搜索的内容....</td></tr>
		<?php else: ?>
			<?php foreach($order_list as $v): ?>
				<tr >
					<td><?= $v['id']?></td>
					<td><?= $v['tel']?></td>
					<td><?= $v['orderNum']?></td>
					<td><?= date('Y-m-d H:i:s',$v['time'])?></td>
					<td><?= $v['amount']?></td>
					  <?php if($v['status']==1): ?>
					      <?php $st='未发货'; ?>
					  <?php elseif($v['status']==2): ?>
                          <?php $st='已发货'; ?>
                      <?php elseif($v['status']==3):  ?>
                          <?php $st='已收货'; ?>
                      <?php endif; ?>
                          
					<td><a href='action.php?bz=status&id=<?=$v['id'] ?>&status=<?= $v['status'] ?>' class='iconfont'><?= $st ?></a></td>

					<td><?= $v['isPay']==1?'未支付':'已支付';     ?></a></td>
					<td><?= $v['orderWay']==1?'货到付款':'在线支付'; ?></td>
					<td>
						<a href="detail.php?id=<?=$v['id']?>">订单详情</a>
						<a href="action.php?bz=del&id=<?= $v['id']?>">删除</a>
					</td>
				</tr>
			<?php endforeach; ?>
      	<?php endif; ?>

		
	</table>
	<?= $page ?>
</body>
</html>