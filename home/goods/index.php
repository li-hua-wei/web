	<?php 
	include '../nav.php';
	//接受分类
	$cid = $_GET['cid'];

	// 根据当前的分类id  查询子分类path中含有当前分类的id  的id
	// $sql = 'select id from category where path like "%'.$cid.'%"';
	// var_dump(query($sql));

	// $cate_id =query($sql);
	// $cate_str ='';
	// foreach($cate_id as $v){
	// 	$cate_str .= $v['id'].',';
	// }

	// $cate_str = rtrim($cate_str,',');
	// var_dump($cate_str);exit;


	// 根据商品的cid 是否在 上述id池里面,  在则	说明该商品属于该类
	$sql = 'select i.icon, g.name, g.desc, g.price, g.sales, g.id
			from  goods g, goodsImg i
			where  g.id = i.gid and face = 1 and up = 1 and g.cid = '.$cid.'
			';
		// echo $sql; exit;
	$goods_list = query($sql);
	// var_dump($goods_list);

?>

	
	<link rel="stylesheet" href="<?= PUB_CSS?>commond.css">
	<link rel="stylesheet" href="<?= CSS?>index.css">

	<div class='main clear'>
	
	<div id='main'>
		

		<div class='main_recommend'>
			<ul>
				<?php foreach($goods_list as $v): ?>
				<li>
					<div class='item_wrap'>
						<div class='item'>
							<div class='item_img'>
								<a href="goods.php?id=<?= $v['id']?>"><img src="<?= img_url($v['icon'],240)?>" alt="" width='240px'></a>
							</div>
							<div class='item_text'>
								<p class='p1'><a href="goods.php?id=<?= $v['id']?>"><?= $v['name']?></a></p>
								<p class='p2'><?= $v['desc']?></p>
								<p class='p3'>
									<span class='s1'>￥<?= $v['price']?></span>
									<span class='s2'>已售 <?= $v['sales']?> 件</span>
								</p>
							</div>
							<a href="<?= URL?>home/goods/goods.php?id=<?= $v['id']?>" class='add_cart'>加入购物车</a>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	</div>

<?php include '../foot.php' ?>