<?php include '../nav.php'; ?>



<link rel="stylesheet" href="<?= CSS?>address.css">
<link rel="stylesheet" href="<?= PUB_CSS?>commond.css">
	<div id='main'>
		<form action="action.php?bz=address" method='post' >
		   <!-- <input type="hidden" name='kk' > -->
			<div style="clear:both"></div>
			<h2 class='cart_title_1'>收获地址</h2>
			<div class='address'>
				<p><span>收货人姓名</span><input type="text" name='receiver' placeholder='Please enter username'></p>
				<p><span>收货人电话</span><input type="text" name='phone' maxlength=11 placeholder='Please enter telephone'></p>
				<p><span>收货人地址</span><input type="text" name='address' placeholder='Please enter address'></p>
			</div>

			<h2 class='cart_title_1'>支付方式</h2>
			<div class='payment'>
				<div class='cod'>
					<label><input type="radio" name="orderWay" value='1' checked>  货到付款 </label>
					<label><input type="radio" name="orderWay" value='2'>  在线支付 </label>
				</div>
			</div>

			<div class="cart_main">
				<h2 class='cart_title'> 确认商品 </h2>
				<div></div>
				<div class='cart_list'>
					<div class='cart_field'>
						<div class='field_name'>商品名称</div>
						<div class='field_price'>数量</div>
						<div class='field_operation'>单价(元)</div>
					</div>
					<div class='cart_pro'>	
						<div class='cart_list_block'>
						<?php foreach($_SESSION['cart'] as $v): ?>
							<div class='cart_item'>
								<div class='goods_name'>
									<div class='pro_img'><a href=""><img src="<?= img_url($v['icon'])?>" width='50px'></a></div>

									<div class='pro_name'><?= $v['name']?></div>
								</div>
								<div class='goods_operation'><input type='text' disabled="disabled" value ="<?= $v['count']?>"></div>
								
								<div class='goods_price'>&yen;<?= $v['price']?></div>
							</div>
							<?php $count= $v['count']; 
							$sum += $count * $v['price'];?>
						<?php endforeach ;?>	

							
							<div class='cart_account'>
								<div class='cart_total'>
									<span class='total1'>商品金额总计: &nbsp;</span>
									<span class='total2'> <?= $sum?> </span>
									<input type="hidden" name='amount' value="<?= $sum?>">
									<span class='total3'> 元 </span>
								</div>
								<div class='cart_btn'>
									<input  type="submit" value='立即下单'>
									<!-- <a href="action.php?bz=address">立即下单</a> -->
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</form>
	</div>

<?php include '../foot.php' ?>