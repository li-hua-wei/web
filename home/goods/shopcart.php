<?php include '../nav.php' ?>
		<link rel="stylesheet" href="<?= PUB_CSS?>commond.css">
	<link rel="stylesheet" href="<?= CSS?>shopcart.css">
	<link rel="stylesheet" href="<?= CSS?>commond.css">

<div id='main'>
		<div class="cart_main">
			<h1 class='cart_title'>我的购物车 <span>在线支付满30元包邮，货到付款满138元包邮</span></h1>
			<div class='cart_list'>
				<!-- 购物车无商品时界面 start -->
					<?php if(empty($_SESSION['cart'])): ?>
					<div class='cart_null_box'>
						<div class='cart_null'>
							<p>购物车暂无商品</p>
							<a href="<?= URL?>home/index.php">我们快去购物吧></a>
						</div>
					</div>
					<?php endif; ?>
				<!-- 购物车无商品时界面 stop -->
					<div class='cart_field'>
						<div class='field_img'>商品图片</div>
						<div class='field_name'>商品名称</div>
						<div class='field_price'>操作</div>
						<div class='field_num'>数量</div>
						<div class='field_operation'>单价(元)</div>
					</div>

					<div class='cart_pro'>
						<div class='cart_list_block'>
						
							<?php foreach($_SESSION['cart'] as $v): ?>
								<div class='cart_item'>
									<div class='goods_name'>
										<div class='pro_img'><a href=""><img src="<?= img_url($v['icon'])?>" alt="" width=50px></a></div>
										<div class='pro_name'><?= $v['name']?></div>
									</div>
									<div class='goods_operation'><a href="action.php?bz=del&id=<?= $v['id']?>">删除</a></div>
									<div class='goods_num'>
										<div class='count_w'>
											<a href="action.php?bz=jian&id=<?= $v['id']?>"><em title="减少数量">-</em></a>
						                    <input type="text" disabled="disabled" value="<?= $v['count']?>">
						                    <a href="action.php?bz=jia&id=<?= $v['id']?>" ><em title="增加数量">+</em></a>
										</div>
									</div>
									<div class='goods_price'>&yen;<?= $v['price']?></div>
								</div>
								<?php  
									$count = $v['count'];
									$sum += $count * $v['price'];
								?>

							<?php endforeach; ?>



							<div class='cart_account'>
								<div class='cart_total'>
									<span class='total1'>商品金额总计: &nbsp;</span>
									<span class='total2'> <?= $sum?> </span>
									<span class='total3'> 元 </span>
								</div>
								<div class='cart_btn'>
									<a href="action.php?bz=orderinfo">去结算</a>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>








	<?php include '../foot.php' ?>