<?php include '../init.php' ; ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>前台注册</title>
	<link rel="stylesheet" type="text/css" href="<?= CSS?>reg.css" />
	<link rel="stylesheet" type="text/css" href="<?= PUB_CSS?>commond.css" />
</head>
<body class='reg-body'>
	<div class='reg-head'>
		<div class='reg-name'>
		<div class='reg-login'>
		<img src="/lamp_shop/public/home/images/logo2.png" alt="">	
		<img src="/lamp_shop/public/home/images/split1.png" alt="">
		<i>华为商城</i>
	</div>
	<div class='reg-g'>
		<h3 class='reg-h3'>注册华为账号</h3>
		<div class='reg-b'>
			<form class='reg-form-edit-area' action="action.php?bz=reg" method='post'>		
				<div class='reg-d1'>
					<div class='reg-d1-l' ><i>手机号:</i></div>
					<div calas='reg-d1-r'>
						<input class='reg-r' type="text" name='tel' maxlength=11>
					</div>
				</div>	
				<div class='reg-d2'>
					<div class='reg-d2-l'><i>密码:</i></div>
					<div calas='reg-d2-r'>
						<input class='reg-r2' type="password" name='pwd' maxlength=20 >
					</div>
				</div>

				<div class='reg-d3'>
					<div class='reg-d3-l'><i>确认密码:</i></div>
					<div calas='reg-d3-r'>
						<input class='reg-r3' type="password" name='repwd'maxlength=20>
					</div>
				</div>


				<div class='reg-d4'>
					<div class='reg-d4-l'><i>验证码:</i></div>
					<div calas='reg-d4-r'>
						<input class='reg-r4' type="text" name='v_code'>
						<div>
							<div class='reg-yzm'><img src="<?= PUB_IMG?>iyzm.php"  onclick="this.src=this.src+'?i='+Math.random()" alt=""></a></a></div>

						</div>
					</div>
				</div>  
				<div class='reg-agree'>
					<input class='reg-ty' type="checkbox" name='ty'><i>同意</i> <u><a href="<?= URL?>404.php">《华为帐号服务条款、华为隐私政策》</a></u>
				</div>
				<div class='reg-submit'>
					<input class='reg-submit1' type="submit" value="立即注册" class='sub'>
				</div>
			</form>
		</div>
	</div>
	<div class='reg-footer '>
		<p>《华为帐号服务条款、华为隐私政策》| 常见问题</p>
		<p>Copyright&copy;2011-2015 华为软件技术有限公司 版权所有 保留一切权利  苏B2-20070200 号 | 苏ICP备09062682号-9</p>
	</div>
</body>
</html>