<?php include '../init.php'; ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>登录页</title>
	<link rel="stylesheet" type="text/css" href="<?= CSS?>login.css" />
	<link rel="stylesheet" type="text/css" href="<?= PUB_CSS?>commond.css" />
	<style>
	/*.login-main .bgi{background-image: url(/lamp_shop/public/home/images/2016-08-09_113956.png);
	}*/
	
	</style>

</head>
<body class='login-body'>
	<div class='login-head'>
		<div class='login-headname'>
			<div class='login'>
			<img src="/lamp_shop/public/home/images/logo2.png" alt="">	
			<img src="/lamp_shop/public/home/images/split1.png" alt="">
			<i>华为商城</i>
			</div>
		</div>
		<div class='login-main'>
			<div class='bgi'></div>
			<h2 class='login-h2'>欢迎登录华为账号</h2>
		<div class='login-form-entry'>
			<form action="action.php?bz=login" method='post'>
				<input  class='login-text'type="text" name='tel' placeholder="手机号" maxlength=11>
				<input class='login-pwd' type="password" name='pwd' placeholder="密码" maxlength=20>
				<!-- <br></br> -->
				<input class='login-jz' type="checkbox" value='记住用户名'><i>记住用户名</i>
				<div class='login-d-wj'><a class='wj' href="">忘记密码</a></div>
				<input class='login-botton' type="submit" value='登录' name='sub'>
			
				<div class='login-state'>
					<span class='login-s1'>使用合作网站帐号登录：</span><br></br>
					<p class='login-p1'>没有华为帐号？<a class='mf' href="reg.php">免费注册</a></p>
					<p class='login-p2'>华为帐号是您用于访问所有华为服务的唯一帐号。使用华为帐号登录后，您将可以使用这台设备的华为云服务、应用市场、华为商城、花粉俱乐部、EMUI产品、华为网盘、开发者联盟。<label class='bable'for="">继续登录表明您已充分理解并同意以上内容。</label></p>
				</div>
				</div>
			</div>
			</form>
		<div>
			<!-- <div class='login-hr-60'></div> -->
			<div class='login-footer'>
				<!-- <p class='login-p-10'></p> -->
				<u><a href="">《华为帐号服务条款、华为隐私政策》</a></u>
				<p>Copyright&copy;2011-2015  华为软件技术有限公司  版权所有  保留一切权利  苏B2-20070200号 | 苏ICP备09062682号-9</p>
			</div>
		</div>
	

	
</body>
</html>