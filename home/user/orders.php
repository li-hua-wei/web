<?php include '../nav.php'; 
	$uid = $_SESSION['home']['id'];

	// var_dump($uid);


	$sql = 'select `orderNum`,`time`,`amount`,`orderWay`,`receiver`,`address`,`phone`,`id`
			from orders
			where id and uid='.$uid;
	// var_dump($sql);exit;
	$ord_list=query($sql);
	// var_dump($ord_list);exit;
?>


<link rel="stylesheet" href="<?= CSS?>space.css">

	<div id='main'>
		<div class='member_left'>
			<h3>
				<a href="<?= URL?>404.php">个人中心</a>
			</h3>
			<div class='order'>
				<h4>我的订单</h4>
				<ul>
					<li><a href="receive.php">待收货</a></li>
					<li><a href="pay.php">待付款</a></li>
					<li><a href="orders.php">全部订单</a></li>
				</ul>
			</div>
			<div class='order'>	
				<h4>账号信息</h4>
				<ul>
					<li><a href="personal.php">个人资料</a></li>
					<li class='receiving'><a href="alter.php">修改密码</a></li>
				</ul>
			</div>
		</div>
		<div class='member_right'>
			<h4>全部订单</h4>
			<div class='goods'>
			<!-- ........................................ -->
					<?php foreach($ord_list as $v): ?> 
					<div class='item'>
						<div class='g_head'>
							<p class='order_status'><?= $v['orderWay']==2?'未付款':'已付款';?></p>
							<div class='order_info'>
								<span>下单时间:<?= date('Y-m-d h:i:s',($v['time']))?></span>
								<span><?= $v['receiver']?></span>
								<span>订单号: <?= $v['orderNum']?></span>
								<span></span>
							</div>
							<div class='order_price'>
								<span>订单金额 : </span>
								<span class='money'><?= $v['amount']?></span>
								<span>元</span>
							</div>
						</div>
						<div class='g_content'>
						<?php

						$sql = 'select i.icon,g.name
						from goods g, goodsImg i,ordersgood og,
						orders o
						 where i.face=1 and g.id=i.gid and g.id=og.gid and o.id=og.oid and og.oid='.$v['id'].'
						';
						// var_dump($sql);exit;
						$list =query($sql);

						
						 foreach($list as $v): ?> 

							<div class='g_content_left'>
								<div class='g_item'>
									<div class='g_img'>
										<img src="<?= img_url($v['icon'])?>" alt="" width='50px'>
									</div>
									<div class='g_info'>
										<ul>
											<li><?= $v['name']?></li>
										</ul>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
							<div class='g_content_right'>
								<a href="">已支付</a>
							</div>
							<div style='clear:both;'></div>
						</div>
					</div>
				<?php endforeach; ?> 
				<!-- ........................... -->
			</div>
		</div>
	</div>

<?php include '../foot.php' ?>
